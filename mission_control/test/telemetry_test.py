from datetime import timedelta
import pathlib
from typing import List
import unittest


from mission_control.src import telemetry


class ThermostatYellowHighFiveMinutes(telemetry.TelemetryRule):
    """Custom rule for yellow high thermostat."""

    @classmethod
    def evaluate(cls, records: List[telemetry.TelemetryRecord]):
        alerts = []
        # fmt: off
        red_highs = [i for i in records
                     if (i.raw_value > i.yellow_high_limit)
                     and i.component == "TSTAT"]
        # fmt: on

        satelites = set([i.satellite_id for i in red_highs])  # dedupe ID
        for satellite in satelites:
            red_highs_sat = [i for i in red_highs if i.satellite_id == satellite]

            # we only need to do work if the number of records is within scope
            if len(red_highs_sat) >= 3:
                # ensure sorted
                sorted_sat = sorted(red_highs_sat, key=lambda x: x.timestamp)

                # for each element we want to look back 2
                for i, red_high_sat in enumerate(sorted_sat):
                    if i > 1:  # skip first 2 element
                        # fmt: off
                        if (sorted_sat[i].timestamp
                            - sorted_sat[i - 2].timestamp
                            < timedelta(minutes=5)
                        ):
                        #fmt: on
                            alerts.append(
                                telemetry.TelemetryAlert(
                                    records=(sorted_sat[i-2], sorted_sat[i-1], sorted_sat[i]),
                                    severity="YELLOW HIGH",
                                    timestamp=sorted_sat[i-2].timestamp,
                                )
                            )
        return alerts


class TestBasicInputData(unittest.TestCase):
    def setUp(self):
        """Load our data."""
        self.filep = pathlib.Path("mission_control/test/example_data.txt")

    def test_delimited_line_parser_pipe_delimeter(self):
        """Ensure the reference fixed width file can be parsed."""
        try:
            dfl = telemetry.DelimitedLineFileParser(self.filep)
            example_data = dfl.consume()
        except Exception as ex:
            self.fail(f"An exception occurred with the test; {ex}")

    def test_monitor_defaults(self):
        """Ensure basic monitoring works as-expected."""
        try:
            telmngr = telemetry.TelemetryManager()
            telmngr.monitor(self.filep, interval=None)

            self.assertNotEqual(len(telmngr.alerts), 0)
            self.assertEqual(len(telmngr.alerts), 2)

        except Exception as ex:
            self.fail(f"An exception occurred with the test; {ex}")


class TestCustomRuleBasicInputData(unittest.TestCase):
    def setUp(self):
        """Load our data."""
        self.filep = pathlib.Path("mission_control/test/example_data.txt")

    def test_monitor_defaults_custom_yellow_high(self):
        """Ensure basic monitoring works as-expected."""
        try:
            rules = [
                ThermostatYellowHighFiveMinutes,  # my custom rule
                telemetry.BatteryVoltageRedLowFiveMinutes,
                telemetry.ThermostatRedHighFiveMinutes,
            ]
            telmngr = telemetry.TelemetryManager(rules=rules)
            telmngr.monitor(self.filep, interval=None)

            self.assertNotEqual(len(telmngr.alerts), 0)
            self.assertEqual(len(telmngr.alerts), 3)

        except Exception as ex:
            self.fail(f"An exception occurred with the test; {ex}")


if __name__ == "__main__":
    # adding convenience for module execution syntax
    # eg. python3 -m mission_control.test.telemetry_test
    unittest.main()
