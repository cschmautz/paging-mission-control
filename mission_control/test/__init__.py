"""Metadata file."""

if __name__ == "__main__":
    # adding convenience for module execution syntax
    # eg. python3 -m mission_control.test
    import unittest

    unittest.main()
