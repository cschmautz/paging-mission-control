"""Main entrypoint for the package."""


import argparse
from datetime import timedelta
from pathlib import Path


from mission_control import TelemetryManager
from mission_control.src import logext


def main():
    parser = argparse.ArgumentParser(
        description="Checks for recent alarms triggered by satellite telemetry"
    )
    parser.add_argument("monitor_feed", type=Path, help="The file to parse")
    parser.add_argument(
        "interval",
        type=int,
        help="Interval in seconds to open and read the file, default is 15s",
    )

    args = parser.parse_args()

    logext.log.debug(f"Parsing telemetry from {args.monitor_feed}")
    tlm = TelemetryManager()
    tlm.monitor(filep=args.monitor_feed, interval=timedelta(seconds=args.interval))


if __name__ == "__main__":
    main()
