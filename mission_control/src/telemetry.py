"""Python namespaces for telemetry-centric classes."""


from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
import logging
import json
from pathlib import Path
import time
from typing import List, Tuple


from mission_control.src import logext


@dataclass
class TelemetryRecord:
    """Stores data about a telemetry event."""

    timestamp: datetime
    satellite_id: str
    red_high_limit: int
    yellow_high_limit: int
    yellow_low_limit: int
    red_low_limit: int
    raw_value: float
    component: str


@dataclass
class TelemetryAlert:
    """Stores data about a telemetry alert."""

    records: Tuple[TelemetryRecord]
    severity: str
    timestamp: datetime

    def __str__(self):
        return json.dumps(
            {
                "sateliteId": self.records[-1].satellite_id,
                "severity": self.severity,
                "component": self.records[-1].component,
                "timestamp": self.timestamp.strftime("%Y-%M-%dT%H:%M:%SZ"),
            },
            indent=2,
        )


class TelemetryRule:
    """Base class as a template for new rules."""

    @classmethod
    def evaluate(cls, records: List[TelemetryRecord]):
        raise NotImplementedError("A concrete class should override this method.")


class BatteryVoltageRedLowFiveMinutes(TelemetryRule):
    """Specific rule for red battery voltage low."""

    @classmethod
    def evaluate(cls, records: List[TelemetryRecord]):
        alerts = []
        # fmt: off
        red_lows = [i for i in records
                    if (i.raw_value < i.red_low_limit)
                    and i.component == "BATT"]
        # fmt: on

        logext.log.debug(
            "len(records): %s, vs len(red_lows): %s", len(records), len(red_lows)
        )

        satelites = set([i.satellite_id for i in red_lows])  # dedupe ID
        for satelite in satelites:
            red_lows_sat = [i for i in red_lows if i.satellite_id == satelite]

            # we only need to do work if the number of records is within scope
            if len(red_lows_sat) >= 3:
                # ensure sorted
                sorted_sat = sorted(red_lows_sat, key=lambda x: x.timestamp)

                for i, red_low_sat in enumerate(sorted_sat):
                    if i > 1:  # skip first 2 element
                        # fmt: off
                        if (sorted_sat[i].timestamp
                            - sorted_sat[i - 2].timestamp
                            < timedelta(minutes=5)
                        ):
                        #fmt: on
                            alerts.append(
                                TelemetryAlert(
                                    records=(sorted_sat[i-2], sorted_sat[i-1], sorted_sat[i]),
                                    severity="RED LOW",
                                    timestamp=sorted_sat[i-2].timestamp,
                                )
                            )
        return alerts


class ThermostatRedHighFiveMinutes(TelemetryRule):
    """Specific rule for red battery voltage low."""

    @classmethod
    def evaluate(cls, records: List[TelemetryRecord]):
        alerts = []
        # fmt: off
        red_highs = [i for i in records
                     if (i.raw_value > i.red_high_limit)
                     and i.component == "TSTAT"]
        # fmt: on

        satelites = set([i.satellite_id for i in red_highs])  # dedupe ID
        for satellite in satelites:
            red_highs_sat = [i for i in red_highs if i.satellite_id == satellite]

            # we only need to do work if the number of records is within scope
            if len(red_highs_sat) >= 3:
                # ensure sorted
                sorted_sat = sorted(red_highs_sat, key=lambda x: x.timestamp)

                # for each element we want to look back 2
                for i, red_high_sat in enumerate(sorted_sat):
                    if i > 1:  # skip first 2 element
                        # fmt: off
                        if (sorted_sat[i].timestamp
                            - sorted_sat[i - 2].timestamp
                            < timedelta(minutes=5)
                        ):
                        #fmt: on
                            alerts.append(
                                TelemetryAlert(
                                    records=(sorted_sat[i-2], sorted_sat[i-1], sorted_sat[i]),
                                    severity="RED HIGH",
                                    timestamp=sorted_sat[i-2].timestamp,
                                )
                            )
        return alerts


class BaseFileParser:
    """Base for all file-based parsers."""

    def __init__(self, filep: Path, line_start: int = 0):
        """Instantiate the parser."""
        self.filep = filep

        self.line_start = line_start  # Seek by line before processing

    def parse(self, *args, **kwargs):
        """Function to call on in child classes to parse the data."""
        raise NotImplementedError("A concrete class should be called upon.")

    def convert_timestamp(self, timestamp: str) -> datetime:
        """Simple converter for timestamps.

        NOTE: space is in UTC; https://solarsystem.nasa.gov/what-time-is-it-in-space/
        """
        return datetime.strptime(timestamp, "%Y%m%d %H:%M:%S.%f").replace(
            tzinfo=timezone.utc
        )

    def consume(self):
        """Main function to run the file parser."""
        records = []
        # NOTE: should we be this cautious?
        # http://python-notes.curiousefficiency.org/en/latest/python3/text_file_processing.html#files-in-an-ascii-compatible-encoding-minimise-risk-of-data-corruption
        with open(
            self.filep.resolve(), mode="r", encoding="ascii", errors="surrogateescape"
        ) as flp:
            for i, line in enumerate(flp):
                if i >= self.line_start:
                    records.append(self.parse(line))
        return records


class DelimitedLineFileParser(BaseFileParser):
    """Consumes a fixed-width line and parses it."""

    def parse(self, line: str, delimeter: str = "|"):
        """Parse a line"""
        parsed = line.split(sep=delimeter)
        return TelemetryRecord(
            timestamp=self.convert_timestamp(parsed[0]),
            satellite_id=parsed[1],
            red_high_limit=int(parsed[2]),
            yellow_high_limit=int(parsed[3]),
            yellow_low_limit=int(parsed[4]),
            red_low_limit=int(parsed[5]),
            raw_value=float(parsed[6]),
            component=parsed[7][:-1],
        )


class JsonParser(BaseFileParser):
    """Consumes a json line and parses it."""

    def parse(self, line: str):
        """Parse a line"""
        parsed = json.loads(line)
        return TelemetryRecord(
            timestamp=self.convert_timestamp(parsed.get("timestamp")),
            satellite_id=parsed.get("satellite_id"),
            red_high_limit=parsed.get("red_high_limit"),
            yellow_high_limit=parsed.get("yellow_high_limit"),
            yellow_low_limit=parsed.get("yellow_low_limit"),
            red_low_limit=parsed.get("red_low_limit"),
            raw_value=parsed.get("raw_value"),
            component=parsed.get("component"),
        )


class TelemetryManager:
    """Stores found records and raises alerts as needed."""

    def __init__(
        self,
        rules: List[TelemetryRule] = [
            BatteryVoltageRedLowFiveMinutes,
            ThermostatRedHighFiveMinutes,
        ],
    ):
        """Init the manager to monitor various file(s)."""

        self.rules = rules
        self.records = []
        self.alerts = []

        # TODO: possible enhancement might be to write cursors to file
        #       for greater flexibility with external daemon managing the
        #       execution. Right now in memory is good enough.
        self.cursors = {}  # Useful for consistent monitoring

    def monitor(
        self,
        filep: Path,
        tzone: timezone = timezone.utc,
        interval: timedelta = timedelta(seconds=15),
        parser: BaseFileParser = None,
    ):
        """Takes in a new record and triggers alarms as needed."""

        # we need to init here so the monitor can track cursors
        if not parser:
            parser = DelimitedLineFileParser

        # set up cursor for the file path
        self.cursors[str(filep)] = 0

        while True:
            parse_it = parser(filep=filep, line_start=self.cursors.get(str(filep), 0))
            # we need to do some time accounting for record scan interval
            start = datetime.now()
            start.replace(tzinfo=tzone)

            new_records = parse_it.consume()
            new_alerts = []
            for rule in self.rules:
                new_alerts.extend(rule.evaluate(new_records))

            logext.log.debug("New records: %s", new_records)
            logext.log.debug("New alerts: %s", new_alerts)

            end = datetime.now()
            end.replace(tzinfo=tzone)

            elapsed = end - start
            logext.log.debug(
                "Time elapsed for parsing new records: %s",
                (end - start).total_seconds(),
            )

            # TODO: determine if we need to keep track of records beyond the alerting;
            # if 'no', then keeping these in memory isn't necessary.
            self.records.extend(new_records)
            self.alerts.extend(new_alerts)
            self.cursors[str(filep)] += len(new_records) - 1

            # alert
            # NOTE: by emitting data and choosing a file log type,
            #       alerts would naturally be preserved there for later review, tailing.
            for alert in new_alerts:
                logext.log.warning("%s", alert)

            # short-circuit for one-time reads, IoC on trigger, tests
            if interval is None:
                break

            to_sleep = (interval - elapsed).total_seconds()
            time.sleep(to_sleep)
