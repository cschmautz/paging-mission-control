"""Centralized logging control for the package.

Manage the logging for the rest of the package here. This logging is configurable
via the configuration files documented in `config.py`.
"""
import logging.config
import os
from pathlib import Path

import sys
import logging
from typing import Union
from pathlib import Path

import yaml


DEFAULT_LOG_CONFIG = {
    "version": 1,
    "disable_existing_loggers": True,
    "root": {"level": "NOTSET", "propogate": True, "handlers": ["console"]},
    "formatters": {
        "simple": {
            "format": "[%(asctime)s] - %(name)s - %(levelname)s - %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
        "verbose": {
            "format": "[%(asctime)s] %(levelname)s <PID %(process)d:%(processName)s> %(name)s.%(funcName)s(): %(message)s"  # noqa: E501
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "WARN",
            "formatter": "simple",
            "stream": "ext://sys.stderr",
        },
        "log_file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "INFO",
            "formatter": "verbose",
            "filename": "/var/log/mission_control.log",
            "maxBytes": 10485760,
            "backupCount": 5,
            "encoding": "utf8",
        },
    },
    "loggers": {
        "mission_control": {
            "level": "INFO",
            "propagate": False,
            "handlers": ["console", "log_file"],
        }
    },
}


def load_config(config_file: str) -> Union[None, list, dict]:
    if (config := Path.home() / ".mission_control" / config_file).exists() or (
        config := Path("/etc/mission_control") / config_file
    ).exists():
        try:
            with open(config, "rt") as f:
                return yaml.safe_load(f.read())
        except yaml.YAMLError:
            logging.exception(f"Could not parse logging config at {config}")
            sys.exit(1)
    else:
        return None


def get_logging_config() -> dict:
    """Loads a logging config from file if available, or a default from const otherwise

    Returns:
        dict: A logger dict config loaded from file or from defaults
    """
    # First thing we need to do is choose which config file to use
    if logging_config := load_config("logging.yml"):
        return logging_config
    else:
        logging_config = DEFAULT_LOG_CONFIG
        # Change logfile to user directory if we're not root since we probably
        # don't want to write to the default /var/log/mission_control.log

        os.makedirs(str(Path.home() / ".mission_control"), exist_ok=True)
        logging_config["handlers"]["log_file"]["filename"] = str(
            Path.home() / ".mission_control/mission_control.log"
        )
        return logging_config


logging.config.dictConfig(get_logging_config())
log = logging.getLogger("mission_control")
