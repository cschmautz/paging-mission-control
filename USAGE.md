# Usage

This section pertains to basic usage of the `mission_control` package, since
 the README is the instructions for the assignment.

## Getting started

Prerequisites for this project / package are:

- Python 3.8+
- Some terminal to run code

## Setting up an environment

This project uses a basic `requirements.txt` for the packaging things, and a
 separate `requirements-dev.txt` for tooling used for the project; install both
 with `pip` like so:

```
pip install -r requirements.txt -r requirements-dev.txt
```

For non-internet access situations, please refer to
 [pip download docs](https://pip.pypa.io/en/stable/cli/pip_download/) for
 pulling packages to be distributed onto non-internet systems.

## Running the tool

In order to run the tool, install `paging_mission_control` following steps
 in this guide, then run:

```
mission_control mission_control/test/example_data.txt 15
```

## Running the tests

This project uses straight `unittest`, so this test suite can be readily
 shipped with the code without issues.

There is a convenience script in the `mission_control/test.__main__` that will
 run the tests like so:

```
python3 -m mission_control.test.telemetry_test -v
```

Since the tests do run the tool itself, there are logs emitted to the terminal
 as well as saved to the default location at
 `~/.mission_control/mission_control.log`.

It should look something like this:

```
test_delimited_line_parser_pipe_delimeter (__main__.TestBasicInputData)
Ensure the reference fixed width file can be parsed. ... ok
test_monitor_defaults (__main__.TestBasicInputData)
Ensure basic monitoring works as-expected. ... [2023-04-08 22:54:35] - mission_control - WARNING - {
  "sateliteId": "1000",
  "severity": "RED LOW",
  "component": "BATT",
  "timestamp": "2018-01-01T23:01:09Z"
}
[2023-04-08 22:54:35] - mission_control - WARNING - {
  "sateliteId": "1000",
  "severity": "RED HIGH",
  "component": "TSTAT",
  "timestamp": "2018-01-01T23:01:38Z"
}
ok
test_monitor_defaults_custom_yellow_high (__main__.TestCustomRuleBasicInputData)
Ensure basic monitoring works as-expected. ... [2023-04-08 22:54:35] - mission_control - WARNING - {
  "sateliteId": "1000",
  "severity": "YELLOW HIGH",
  "component": "TSTAT",
  "timestamp": "2018-01-01T23:01:38Z"
}
[2023-04-08 22:54:35] - mission_control - WARNING - {
  "sateliteId": "1000",
  "severity": "RED LOW",
  "component": "BATT",
  "timestamp": "2018-01-01T23:01:09Z"
}
[2023-04-08 22:54:35] - mission_control - WARNING - {
  "sateliteId": "1000",
  "severity": "RED HIGH",
  "component": "TSTAT",
  "timestamp": "2018-01-01T23:01:38Z"
}
ok
```

## Adding custom rules

`mission_control` exposes some basic building blocks for extending the base
 functionality, including new `TelemetryRule` as well as customized
 `TelemetryManager`.

## Building the package for distribution

First, set up an environment that has the necessary dependencies from the
 bundled `requirements.txt` file.

In order to build this code, from the target installation machine / OS:

```
python3 setup.py sdist
```

This should produce a source install tgz of the `paging_mission_control` package to be installed
 on the target machines / satellites.

The included `MANIFEST.in` bundles the base test input file; the base case
 test suite can be validated on the target system.

To install the built package, use:

```
pip install --find-links dist/ --no-index --no-cache paging_mission_control
```
