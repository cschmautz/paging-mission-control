from setuptools import setup

setup(
    name="paging-mission-control",
    version="0.0.1",
    packages=["mission_control", "mission_control.src", "mission_control.test"],
    url="https://gitlab.com/cschmautz/paging-mission-control",
    license="proprietary",
    author="Chris Schmautz",
    author_email="none@none.com",
    description="Simple alerting engine for satellite telemetry data",
    long_description="USAGE.md",
    long_description_content_type="text/markdown",
    include_package_data=True,
    install_requires=["PyYAML"],
    entry_points={"console_scripts": ["mission_control=mission_control.__main__:main"]},
)
